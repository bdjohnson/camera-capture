/*
     File: AVCamAppDelegate.m
 Abstract: Application delegate.
  Version: 3.1
 
 Copyright (C) 2014 Apple Inc. All Rights Reserved.
 
 */

#import "AVCamAppDelegate.h"

@implementation AVCamAppDelegate

@end
