//
//  AppDelegate.h
//  TimeHistory
//
//  Created by Bruce Johnson on 10/25/14.
//  Copyright (c) 2014 Motiv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

