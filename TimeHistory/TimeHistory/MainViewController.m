//
//  ViewController.m
//  TimeHistory
//
//  Created by Bruce Johnson on 10/25/14.
//  Copyright (c) 2014 Motiv. All rights reserved.
//

#import "MainViewController.h"
#import "MVTimeHistoryView.h"

@interface MainViewController () <MVTimeHistoryViewDataSourceProtocol>

@property (weak, nonatomic) IBOutlet MVTimeHistoryView *timeHistoryView;
@property (readwrite, assign) CGFloat slideValue;
@property (weak, nonatomic) IBOutlet UISlider *valueSlider;

- (IBAction)toggleTimeHistory:(id)sender;
- (IBAction)handleSliderEvent:(id)sender;

@end

@implementation MainViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.timeHistoryView.datasource = self;
	//set the maximum amount of points to display along the x axis.
	//A smaller number will cause the time history plot to move faster, a larger number will make it appear to move slower.
	self.timeHistoryView.maximumPoints = 150;
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear: animated];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (IBAction)toggleTimeHistory:(id)sender
{
	UIButton *button = (UIButton *)sender;
	
	if (self.timeHistoryView.periodicUpdateIsRunning == NO) {
		//start display clock
		[self.timeHistoryView startPerodicUpdate];
		[button setTitle: @"Stop" forState: UIControlStateNormal];
	} else {
		//stop display clock
		[self.timeHistoryView stopPeriodicUpdate];
		[button setTitle: @"Start" forState: UIControlStateNormal];
	}
}

- (IBAction)handleSliderEvent:(id)sender
{
	UISlider *slider = (UISlider *)sender;
	self.slideValue = slider.value;
}

#pragma mark - MVTimeHistoryViewDataSourceProtocol

- (void) viewDidStartUpdating
{
	self.valueSlider.hidden = NO;
}

//called on a background thread
- (BOOL) updateHistory: (NSArray **)array;
{
	//FIXME:  should to a better job of locking the slider value since it is being queried and written to on seperate threads.
	CGFloat valueToBePlotted = self.slideValue;
	
	*array = @[@(valueToBePlotted), @(valueToBePlotted * 0.24)];
	
	return YES;
}

- (void) viewDidStopUpdating
{
	self.valueSlider.hidden = YES;
}
@end
