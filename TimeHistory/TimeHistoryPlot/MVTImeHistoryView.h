/*
    File: MVTimeHistoryView.h
Abstract: A custom view for plotting history values.
*/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol MVTimeHistoryViewDataSourceProtocol <NSObject>

/*
 called when the display clock start running
 */
- (void) viewDidStartUpdating;

/*
 called on a 20Hz periodic basis.  Used to get data to be plotted
 
 will pass a nil pointer to an NSArray which will need to be created an populated by the datasource object.
 The NSArray is an array of NSNumbers to be plotted on the Y axis
 
 The datasource returns a BOOL which will trigger a plotting cycle
 
 NOTE: this method will be called on a thread other than the main thread.
 */
- (BOOL) updateHistory: (NSArray **)array;

/*
 called when the display clock stops running
 */
- (void) viewDidStopUpdating;

@end

@interface MVTimeHistoryView : UIView

/*
 the plotting data source object that conforms to the MVTimeHistoryViewDataSource Protocol
 */
@property (nonatomic, readwrite, assign) IBOutlet id<MVTimeHistoryViewDataSourceProtocol>datasource;

/*
 The maximum amount of plot points to be displayed along the x axis.  A larger number will move the time history plot more slowly.  A smaller number will move it more quickly
 */
@property (readwrite, assign) NSUInteger maximumPoints;

/*
 an offset value is added to each NSNumber to be plotted
 */
@property (readwrite, assign) CGFloat offset;

/*
 a scale value is multiplied by each NSNumber to be plotted
 */
@property (readwrite, assign) CGFloat scale;

/*
 query if the diplay clock is running
 */
@property (readonly) BOOL periodicUpdateIsRunning;

/*
 call to start the plotting and datasource callbacks
 */
- (void) startPerodicUpdate;

/*
 call to stop the plotting and datasource callbacks
 */
- (void) stopPeriodicUpdate;

@end

