/*
    File: MVTimeHistoryView.m
Abstract: A custom view for plotting history values.
*/

#import "MVTimeHistoryView.h"

#define MAX_HISTORY_X_PTS 150
#define TWENTY_HERTZ 0.03333333
#define TEN_HERTZ 0.06666666

@interface MVTimeHistoryView ()

- (void) commonInit;
- (void) periodicUpdateCallback: (CADisplayLink *)aDisplayLink;

@end

@implementation MVTimeHistoryView
{
	CADisplayLink *_displayLink;
	CFTimeInterval _twentyHzTimeStamp;
	
	/*
	 the queue (and thread) that will handle the data source callbacks
	 */
	dispatch_queue_t _periodicQueue;

	NSUInteger _numberOfPlotLines;
	NSUInteger _nextXValueIndex;
	
	/*
	 The time history plot allows for a maximum of 512 plot points along the X axis and a total of 8 seperate plot lines
	 adjust the dimension of this array to change those maximum numbers
	 */
	CGFloat _history[512][8];
}

- (id) initWithCoder: (NSCoder *)aDecoder
{
	self = [super initWithCoder: aDecoder];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (id) initWithFrame:(CGRect)frame
{
	self = [super initWithFrame: frame];
	if (self) {
		[self commonInit];
	}
	return self;
}

- (void) commonInit
{
	_periodicQueue = dispatch_queue_create("MVTimeHistoryView.periodicQueue", DISPATCH_QUEUE_SERIAL);

	self.layer.borderColor = [UIColor grayColor].CGColor;
	self.layer.borderWidth = 1.0f;
	self.offset = 0.0f;
	self.scale = 1.0;
	_periodicUpdateIsRunning = NO;
	self.maximumPoints = 150;
}

- (void) dealloc
{
	[_displayLink invalidate];
}

- (void) startPerodicUpdate
{
	if (_displayLink == nil)
	{
		_twentyHzTimeStamp = CACurrentMediaTime();
		_displayLink = [CADisplayLink displayLinkWithTarget: self selector: @selector(periodicUpdateCallback:)];
		[_displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
		_periodicUpdateIsRunning = YES;
		[self.datasource viewDidStartUpdating];
	}
}

- (void) stopPeriodicUpdate
{
	[_displayLink invalidate];
	_displayLink = nil;
	_periodicUpdateIsRunning = NO;
	[self.datasource viewDidStopUpdating];
}

- (void) periodicUpdateCallback: (CADisplayLink *)aDisplayLink
{
	CFTimeInterval deltaTwentyHzTime = aDisplayLink.timestamp - _twentyHzTimeStamp;
	
	if (deltaTwentyHzTime >= TWENTY_HERTZ)
	{
		__block MVTimeHistoryView *view = self;
		
		dispatch_async(_periodicQueue, ^{
			NSArray *array = nil;
			BOOL result = [view.datasource updateHistory: &array];
			if (result && array != nil) {
				dispatch_async(dispatch_get_main_queue(), ^{
					_numberOfPlotLines = array.count;
					for (NSUInteger i = 0; i < _numberOfPlotLines; i++) {
						// Add to history.
						CGFloat value = [array[i] floatValue];
						_history[_nextXValueIndex][i] = value * view.scale + view.offset;
					}
					// Advance the X axis index counter, rollover if needed.
					_nextXValueIndex = (_nextXValueIndex + 1) % view.maximumPoints;
					// Mark ourself as needing to be redrawn.
					[view setNeedsDisplay];
				});
			}
		});
		_twentyHzTimeStamp = CACurrentMediaTime();
	}
}

- (void)drawGraphInContext:(CGContextRef)context withBounds:(CGRect)bounds
{
    CGFloat value, temp;

    // Save any previous graphics state settings before setting the color and line width for the current draw.
    CGContextSaveGState(context);
	CGContextSetLineWidth(context, 1.0);

	// Draw the intermediate lines
	CGContextSetGrayStrokeColor(context, 0.6, 1.0);
	CGContextBeginPath(context);
	for (value = -5 + 1.0; value <= 5 - 1.0; value += 1.0) {
	
		if (value == 0.0) {
			continue;
		}
		temp = 0.5 + roundf(bounds.origin.y + bounds.size.height / 2 + value / (2 * 5) * bounds.size.height);
		CGContextMoveToPoint(context, bounds.origin.x, temp);
		CGContextAddLineToPoint(context, bounds.origin.x + bounds.size.width, temp);
	}
	CGContextStrokePath(context);
	
	// Draw the center line
	CGContextSetGrayStrokeColor(context, 0.25, 1.0);
	CGContextBeginPath(context);
	temp = 0.5 + roundf(bounds.origin.y + bounds.size.height / 2);
	CGContextMoveToPoint(context, bounds.origin.x, temp);
	CGContextAddLineToPoint(context, bounds.origin.x + bounds.size.width, temp);
	CGContextStrokePath(context);

    // Restore previous graphics state.
    CGContextRestoreGState(context);
}

- (void)drawHistory:(NSUInteger)axis fromIndex:(NSUInteger)index inContext:(CGContextRef)context bounds:(CGRect)bounds
{
    CGFloat value;
	    
	CGContextBeginPath(context);
    for (NSUInteger counter = 0; counter < self.maximumPoints; ++counter)
	{
        // UIView referential has the Y axis going down, so we need to draw upside-down.
        value = _history[(index + counter) % self.maximumPoints][axis] / -1;
        if (counter > 0) {
            CGContextAddLineToPoint(context, bounds.origin.x + (CGFloat)counter / (CGFloat)(self.maximumPoints - 1) * bounds.size.width, bounds.origin.y + bounds.size.height / 2 + value * bounds.size.height / 2);
        } else {
            CGContextMoveToPoint(context, bounds.origin.x + (CGFloat)counter / (CGFloat)(self.maximumPoints - 1) * bounds.size.width, bounds.origin.y + bounds.size.height / 2 + value * bounds.size.height / 2);
        }
    }
    // Save any previous graphics state settings before setting the color and line width for the current draw.
    CGContextSaveGState(context);
    CGContextSetRGBStrokeColor(context, (axis == 0 ? 1.0 : 0.0), (axis == 1 ? 1.0 : 0.0), (axis == 2 ? 1.0 : 0.0), 1.0);
	CGContextSetLineWidth(context, 1.0);
    CGContextStrokePath(context);
    // Restore previous graphics state.
    CGContextRestoreGState(context);
}

- (void)drawRect:(CGRect)clip
{
    NSUInteger index = _nextXValueIndex;
    
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGRect bounds = CGRectMake(0, 0, [self bounds].size.width, [self bounds].size.height);
	
	// create the graph
	[self drawGraphInContext:context withBounds:bounds];
	
    // plot points with anti-aliasing turned off
    CGContextSetAllowsAntialiasing(context, false);
    for (NSUInteger i = 0; i < _numberOfPlotLines; ++i)
	{
		[self drawHistory:i fromIndex:index inContext:context bounds:bounds];
    }
    CGContextSetAllowsAntialiasing(context, true);
}


@end
